# README #

This profile is created specifically for PowerShell 7. **NOT** Windows PowerShell 5. However the scripts inside will likely work with PS5 as well.

## Setup

### Module Requirements
If you intend on using the included module imports you'll need to install the following:

- [posh-git](https://github.com/dahlbyk/posh-git)
- [oh-my-posh](https://ohmyposh.dev/)
    - This also uses my personal posh theme located [here](https://bitbucket.org/TNichols1/workspace/snippets/B9xExA/oh-my-posh-theme).

### Install
- Create a temp folder and clone the project inside.
- Copy `\Scripts` and `\.git` folders into your own `~\Documents\PowerShell` folder.
    - Either completely copy the `Microsoft.PowerShell_profile.ps1` file, or cherry pick lines of code from it into your own established profile.

- Re-open PowerShell 7.


### Docker Setup
Download Docker Desktop
	https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe
	
Install WSL 2 and Ubuntu
	PS> wsl2 --install

Switch to Windows containers...

Update Docker Engine setting with this config:
{
  "experimental": true
}

If you recieve an error "Invalid host" while building a Docker container follow steps here to fix you host network priority
https://github.com/docker/for-win/issues/6453#issuecomment-925532885