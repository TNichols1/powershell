# --- Imports --------
if (-not (Get-Module -ListAvailable -Name posh-git)) {
    "Installing posh-git"
    Install-Module posh-git
}
Import-Module posh-git

if (-not (Get-Module -ListAvailable -Name oh-my-posh)) {
    "Installing oh-my-posh"
    Install-Module oh-my-posh
}
Import-Module oh-my-posh

if (-not (Get-Module -ListAvailable -Name PSMenu)) {
    "Installing PSMenu"
    Install-Module PSMenu
}
Import-Module PSMenu

if (-not (Get-Module -ListAvailable -Name powershell-yaml)) {
    "Installing powershell-yaml"
    Install-Module powershell-yaml -AllowClobber
}
Import-Module powershell-yaml

$curPath = Split-Path $MyInvocation.MyCommand.Path -Parent
# --- Global Directory Settings ----
$global:wkspDir = "C:\Workspace"
$global:ofDir = ${wkspDir}+"\OF"
$global:ofDevDir = ${ofDir}
$global:ofDockerDir = ${curPath}+"\OFDocker"

# --- Cache Settings ----
$global:repoMenuCacheExpireMin = 43800 #1 month

# --- Environment Vars ----
$env:OFDOCKER_VSRemoteDebugFolder = "C:\Program Files\Microsoft Visual Studio\2022\Professional\Common7\IDE\Remote Debugger"
$env:OFDOCKER_OfWorkspaces = $global:ofDir
$env:OFDOCKER_OfDBDirectory = ${ofDir} + "\database"
$env:OFDOCKER_OfMDF = "of_main_new.mdf"
$env:OFDOCKER_OfLDF = "of_main_new.ldf"

# --- My Imports -----
$commands = ${curPath} + "\Scripts\of-commands"

Import-Module $commands\cache.ps1
Import-Module $commands\utl.ps1

Import-Module $commands\wsl.ps1
Import-Module $commands\nt.ps1
Import-Module $commands\dir.ps1
Import-Module $commands\git.ps1
Import-Module $commands\of.ps1
Import-Module $commands\iis.ps1
Import-Module $commands\chains.ps1

# Clear some space!
clear
