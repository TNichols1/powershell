# Quick PowerShell commands for notes.

New-Module nt-commands {

# Functions ---------------------------
  # vim scratchpad for running notes.
  function nt.cat {
    cat ~/note
  }
  # choco install vim
  function nt.vim {
    vim ~/note
  }
# Functions ---------------------------


# Exports -----------------------------
  Export-ModuleMember nt.cat
  Export-ModuleMember nt.vim
# Exports -----------------------------

} | Import-Module