# Quick change directory commands.

New-Module dir-commands {
# Functions ---------------------------
  # Quick Explorer.
  function expl { explorer . }

  # Base workspace directory.
  function wksp { cd ${wkspDir} }

  # /of/dev/ directories.
  function of.dev.Log         { cd ${ofDevDir}\of-logging }
  function of.dev.Bol         { cd ${ofDevDir}\of-bol }
  function of.dev.CoreService { cd ${ofDevDir}\of-core-service-clients }
  function of.dev.Fax         { cd ${ofDevDir}\of-fax }
  function of.dev.FaxService  { cd ${ofDevDir}\of-fax-image-service }
  function of.dev.ScimsCore   { cd ${ofDevDir}\of-scims.core }
  function of.dev.Scims       { cd ${ofDevDir}\of-scims }
  function of.dev.Ice         { cd ${ofDevDir}\of-ice }
  function of.dev.ITools      { cd ${ofDevDir}\of-itools }
  function of.dev.Core        { cd ${ofDevDir}\of-core }
  function of.dev.Main        { cd ${ofDevDir}\of-main }

  # /of/staging/ directories.
  function of.staging.Main    { cd ${ofStagingDir}\of-main }

  # /of/auth0/ directories.
  function of.auth0.Main      { cd ${ofAuthDir}\of-main }
# Functions ---------------------------


# Exports -----------------------------
  # Quick Explorer.
  Export-ModuleMember expl

  # Base workspace directory.
  Export-ModuleMember wksp

  # /of/dev/ directories.
  Export-ModuleMember of.dev.Log
  Export-ModuleMember of.dev.Bol
  Export-ModuleMember of.dev.CoreService
  Export-ModuleMember of.dev.Fax
  Export-ModuleMember of.dev.FaxService
  Export-ModuleMember of.dev.ScimsCore
  Export-ModuleMember of.dev.Scims
  Export-ModuleMember of.dev.Ice
  Export-ModuleMember of.dev.ITools
  Export-ModuleMember of.dev.Core
  Export-ModuleMember of.dev.Main

  # /of/staging/ directories.
  Export-ModuleMember of.staging.Main

  # /of/auth0/ directories.
  Export-ModuleMember of.auth0.Main
# Exports -----------------------------

} | Import-Module