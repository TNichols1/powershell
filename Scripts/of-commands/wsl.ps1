# Quick PowerShell commands for Windows Subsystem for Linux.

New-Module wsl-commands {

# Functions ---------------------------
  # matrix display
  function wsl.cm {
    wsl cmatrix
  }
# Functions ---------------------------


# Exports -----------------------------
  Export-ModuleMember wsl.cm
# Exports -----------------------------

} | Import-Module