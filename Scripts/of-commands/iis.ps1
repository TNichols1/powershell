# Quick PowerShell commands for IIS.

New-Module iis-commands {

# Functions ---------------------------
  # Import necessary modules.
  function iis.init {
    Import-Module IISAdministration
    Import-Module WebAdministration

    # Display current status.
    iis.Get
  }
  # Start/Stop functions - Using IIS default values.
  function iis.Start {
    Start-IISSite -Name "Default Web Site"
    Start-WebAppPool -Name "DefaultAppPool"
  }
  function iis.Stop {
    Stop-IISSite -Name "Default Web Site"
    Stop-WebAppPool -Name "DefaultAppPool"
  }

  # Get info.
  function iis.Get {
    utl.Write "App Pool: DefaultAppPool"
    Get-IISAppPool "DefaultAppPool"

    utl.Write "Website: Default Web Site"
    Get-IISSite "Default Web Site"
  }
# Functions ---------------------------


# Exports -----------------------------
  Export-ModuleMember iis.Init
  Export-ModuleMember iis.Start
  Export-ModuleMember iis.Stop

  Export-ModuleMember iis.Get
# Exports -----------------------------

} | Import-Module