# Quick utility PowerShell commands.

New-Module utl-commands {

# Functions ---------------------------
  # Consistent console write command.
  function utl.Write {
    [CmdletBinding()]
    param ([string]$status)

    Write-Host " >_ "$status" " -ForegroundColor black -BackgroundColor green
  }

  function utl.remove.Path {
    [CmdletBinding()]
    param ([string]$pathName)

    if (Test-Path $pathName) {
      utl.Write "Removing $($pathName)"
      Remove-Item -Recurse -Force $pathName
    }
  }

  function utl.remove.File {
    [CmdletBinding()]
    param ([string]$fileName)

    if (Test-Path $fileName) {
      utl.Write "Removing $($fileName)"
      Remove-Item $fileName
    }
  }

  function utl.clean.AllPackages {
    utl.remove.Path ~\AppData\Local\Temp\.net
    utl.remove.Path ~\AppData\Local\Temp\WebCompiler*
  }

# Functions ---------------------------


# Exports -----------------------------
  Export-ModuleMember utl.Write

  Export-ModuleMember utl.remove.Path
  Export-ModuleMember utl.remove.File

  Export-ModuleMember utl.clean.AllPackages
# Exports -----------------------------

} | Import-Module