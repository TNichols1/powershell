# Quick git commands.

New-Module git-commands {

# Functions ---------------------------
  # git branch shortcuts
  function gb.develop      { git checkout develop }
  function gb.develop.del  { git branch -D develop }
  function gb.testing      { git checkout testing }
  function gb.testing.del  { git branch -D testing }

  # git shortcuts
  function g.Fetch { git fetch --tags --prune }
  function g.Branches { git branch -a }
  function g.StashList { git stash list }
  function g.Status { git status }
  function g.Log { git log }
  function g.Restore { git restore * }
  function g.Restore.AssemblyInfo { git restore **/AssemblyInfo.* }
  function g.Checkout {
    [CmdletBinding()]
    param ([string]$branchName)

    git checkout $branchName
  }
  function g.Pull {
    [CmdletBinding()]
    param ([string]$branchName)

    git pull origin $branchName
  }
  function g.NewBranch {
    [CmdletBinding()]
    param ([string]$branchName)

    git checkout -b $branchName
  }

  # Command help
  function g.Shortcuts {
    echo "-- Git Commands: ---"
    echo "g.Fetch     - 'git fetch --tags --prune'"
    echo "g.Branches  - 'git branch -a'"
    echo "g.StashList - 'git stash list'"
    echo "g.Status    - 'git status'"
    echo "g.Log       - 'git log'"
    echo "g.Restore   - 'git restore *'"
    echo "g.Restore   - 'git restore **/AssemblyInfo.*'"
    echo "g.Checkout  - 'git checkout [branchName]'"
    echo "g.Pull      - 'git pull origin [branchName]'"
    echo "g.NewBranch - 'git checkout -b [branchName]'"
    echo ""
  }
# Functions ---------------------------


# Exports -----------------------------
  # git branch shortcuts
  Export-ModuleMember gb.develop
  Export-ModuleMember gb.develop.del
  Export-ModuleMember gb.testing
  Export-ModuleMember gb.testing.del

  # git shortcuts
  Export-ModuleMember g.Fetch
  Export-ModuleMember g.Branches
  Export-ModuleMember g.Status
  Export-ModuleMember g.StashList
  Export-ModuleMember g.Log
  Export-ModuleMember g.Restore
  Export-ModuleMember g.Restore.AssemblyInfo
  Export-ModuleMember g.Checkout
  Export-ModuleMember g.Pull
  Export-ModuleMember g.NewBranch

  # Command help
  Export-ModuleMember g.Shortcuts
# Exports -----------------------------

} | Import-Module