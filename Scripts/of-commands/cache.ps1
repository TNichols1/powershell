class CacheItem {
    [string]$Key
    [object]$Value
    [datetime]$Expire
}

class FileCache
{
    static [HashTable]$Items
    static [string]$SaveLocation = "$($env:TEMP)\OFPowershell\profile.cache.xml"

    static FileCache()
    {
        #load
        try {
            [FileCache]::Items = import-clixml -path ([FileCache]::SaveLocation)
        }
        catch {
            [FileCache]::Items = @{}
        }
    }

    static [void] Set([string]$key, [object]$value, [int]$expiremin)
    {

        [CacheItem] $item = [CacheItem]::new()
        $item.Key = $key
        $item.Value = $value
        $item.Expire = (Get-Date).AddMinutes($expiremin)
        
        [FileCache]::Items[$key] = $item
        [FileCache]::Save()
    }

    static [object] Get([string]$key)
    {

        $item = [FileCache]::Items[$key]
        if($null -eq $item)
        {
            return $null
        }
        if($item.Expire -gt (Get-Date))
        {
            return $item.Value
        }
        else
        {
            [FileCache]::Items.Remove($key)
            [FileCache]::Save()
            return $null
        }
    }

    static [void] Save() 
    {
        #save
        $dir = Split-Path ([FileCache]::SaveLocation)
        if((Test-Path $dir) -eq $false)
        {
            New-Item -Path $dir -ItemType Directory
        }
        export-clixml -InputObject ([FileCache]::Items) -path ([FileCache]::SaveLocation)
    }

    static [HashTable] Dump() {
        return [FileCache]::Items
    }

    static [void] Clear() {
        [FileCache]::Items = @{}
        [FileCache]::Save()
    }
}