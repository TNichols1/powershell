class RepoDefinition
{
    [string]$Name
    [System.Type]$Type
    [string]$Directory
    [string]$CloneSSH
    [string[]]$Dependencies
    [int]$Index
    [string]$Description
    [string[]]$Containers

    static [RepoDefinition[]]$List = @(
        [RepoDefinition]::new("logging",[LibRepository],"${ofDevDir}\of-logging", "https://bitbucket.org/scisolutions/of-logging.git", "The of-logging repository", @()),
        [RepoDefinition]::new("bol",[LibRepository],"${ofDevDir}\of-bol", "https://bitbucket.org/scisolutions/of-bol.git", "The of-bol repository", @("logging")),
        [RepoDefinition]::new("service-clients",[LibRepository],"${ofDevDir}\of-core-service-clients", "https://bitbucket.org/scisolutions/of-core-service-clients.git", "The of-core-service-clients repository", @()),
        [RepoDefinition]::new("core",[LibRepository],"${ofDevDir}\of-core", "https://bitbucket.org/scisolutions/of-core.git", "The of-core repository", @("bol", "service-clients")),
        [RepoDefinition]::new("main",[ApplicationRepository],"${ofDevDir}\of-main", "https://bitbucket.org/scisolutions/of-main.git", "The of-main repository", @("bol", "service-clients", "core"), @("order-entry", "admin", "imigrate", "imigrate-dest")),
        [RepoDefinition]::new("itools",[ApplicationRepository],"${ofDevDir}\of-itools", "https://bitbucket.org/scisolutions/of-itools.git", "The of-itools repository", @("bol")),
        [RepoDefinition]::new("order-api",[ApplicationRepository],"${ofDevDir}\order-api", "https://bitbucket.org/scisolutions/order-api.git", "The order-api repository", @("bol", "service-clients", "core"), @("order-api")),
        [RepoDefinition]::new("ice",[ApplicationRepository],"${ofDevDir}\of-ice", "https://bitbucket.org/scisolutions/of-ice.git", "The of-ice repository", @("bol", "service-clients", "core")),
        [RepoDefinition]::new("fax",[ApplicationRepository],"${ofDevDir}\of-fax", "https://bitbucket.org/scisolutions/of-fax.git", "The of-fax repository", @("bol")),
        [RepoDefinition]::new("fax-service",[ApplicationRepository],"${ofDevDir}\of-fax-image-service", "https://bitbucket.org/scisolutions/of-fax-image-service.git", "The of-fax-image-service repository", @()),
        [RepoDefinition]::new("scims",[ApplicationRepository],"${ofDevDir}\of-scims", "https://bitbucket.org/scisolutions/of-scims.git", "The of-scims repository", @("bol", "scims-core")),
        [RepoDefinition]::new("scims-core",[LibRepository],"${ofDevDir}\of-scims.core", "https://bitbucket.org/scisolutions/of-scims.core.git", "The of-scims.core repository", @("bol")),
        [RepoDefinition]::new("auth",[ApplicationRepository],"${ofDevDir}\of-auth", "https://bitbucket.org/scisolutions/of-auth.git", "The of-auth repository", @("bol"), @()),
        [RepoDefinition]::new("looker-api-client",[LibRepository],"${ofDevDir}\looker.apiclient", "https://bitbucket.org/scisolutions/looker.apiclient.git", "The looker.apiclient repository", @(), @()))

        RepoDefinition([string] $name, [System.Type]$type, [string]$directory, [string]$cloneSSH, [string]$description, [string[]]$dependencies)
        {
            $this.Name = $name
            $this.Type = $type
            $this.Directory = $directory
            $this.CloneSSH = $cloneSSH
            $this.Description = $description
            $this.Dependencies = $dependencies
            $this.Containers = @()
        }

        RepoDefinition([string] $name, [System.Type]$type, [string]$directory, [string]$cloneSSH, [string]$description, [string[]]$dependencies, [string[]]$containers)
        {
            $this.Name = $name
            $this.Type = $type
            $this.Directory = $directory
            $this.CloneSSH = $cloneSSH
            $this.Description = $description
            $this.Dependencies = $dependencies
            $this.Containers = $containers
        }

    [RepoDefinition] Clone()
    {
        return [RepoDefinition]::new($this.Name, $this.Type, $this.Directory, $this.CloneSSH, $this.Description, $this.Dependencies, $this.Containers)
    }

    [BaseProcess] GenerateObject([string[]]$cmds)
    {
        return (New-Object $this.Type -ArgumentList $cmds, $this.Name, $this.Directory, $this.CloneSSH, $this.Dependencies, $this.Containers)
    }

    static [BaseProcess] GenerateObjectByName([string]$name, [string[]]$cmds)
    {
        [RepoDefinition] $def = [RepoDefinition]::List | Where-Object Name -eq $name
        return $def.GenerateObject($cmds)
    }

    static [BaseProcess[]] ParseArgumentList([string[]]$cmds)
    {
        #Clone a RepoDefinition for each argument that matches a repo name in the list
        $indexedRepoDefs = @()
        foreach ($repoDef in [RepoDefinition]::List)
        {
            $is = (0..($cmds.Count-1)) | Where-Object {$cmds[$_] -eq $repoDef.Name}
            $is | ForEach-Object {
                if($_ -gt -1)
                {
                    $clone = $repoDef.Clone()
                    $clone.Index = $_
                    $indexedRepoDefs += $clone
                }
            }
        }

        #Sort the list by their Index
        $indexedRepoDefs = $indexedRepoDefs | Sort-Object Index

        #return list
        [BaseProcess[]]$repoCmds = @()

        #if the first cmd is not a repo name
        $lastbasecmd = If ($indexedRepoDefs.Count -gt 0) {$indexedRepoDefs[0].Index - 1} Else {$cmds.Count - 1}
        if($lastbasecmd -gt -1)
        {
            #try to generate a ApplicationRepository based on the current directory
            $repoCmds += [RepoDefinition]::GetRepoFromCurrentDir($cmds[0..$lastbasecmd])
        }

        #Loop through the indexes and generate Process objects for each repo in the argument list
        for ($i = 0; $i -lt $indexedRepoDefs.Count; $i++)
        {
            $end = $cmds.Count - 1
            $start = $indexedRepoDefs[$i].Index + 1
            if(($i+1) -ne $indexedRepoDefs.Count)
            {
                $end = $indexedRepoDefs[$i+1].Index -1
            }

            $newCmds = If ($start -le $end) {$cmds[$start..$end]}
            $repoCmds += $indexedRepoDefs[$i].GenerateObject($newCmds)
        }

        #make sure there is at elast one Process to return
        if($repoCmds.Count -eq 0)
        {
            $repoCmds += [RepoDefinition]::GetRepoFromCurrentDir($null)
        }

        return $repoCmds
    }

    hidden static [BaseProcess] GetRepoFromCurrentDir([string[]]$cmds)
    {
        #try to generate a ApplicationRepository based on the current directory
        $repoDef = ([RepoDefinition]::List | Where-Object Directory -eq (Get-Location))
        if($null -ne $repoDef)
        {
            return $repoDef.GenerateObject($cmds)
        }
        else
        {
            #the curret dir is not a repo, default to a BaseProcess
            return [BaseProcess]::new($cmds, @())
        }
    }

    static [scriptblock] Dump()
    {
        return {
            [RepoDefinition]::List |  Format-Table -Property @{Expression=" ";Width=3}, @{Expression={$_.Name};Width=30}, Description -HideTableHeaders -Wrap
        }
    }
}

class Command
{
    [string[]] $CommandList
    [System.Management.Automation.PSMethod] $MethodCall
    [string] $Description

    Command([string[]]$commandList, [System.Management.Automation.PSMethod]$methodCall, [string] $description)
    {
        $this.CommandList = $commandList
        $this.MethodCall = $methodCall
        $this.Description = $description
    }

    [scriptblock[]] GetScript()
    {
        return $this.MethodCall.Invoke()
    }
}

class CommandAlias
{
    [string]$Name
    [string]$Cmds

    static [HashTable]$List
    static [string]$SaveLocation = "$($env:TEMP)\OFPowershell\commandaliaslist.xml"

    static CommandAlias()
    {
        #load
            try {
                [CommandAlias]::List = import-clixml -path ([CommandAlias]::SaveLocation)
            }
            catch {
                [CommandAlias]::List = @{}
            }
    }

    CommandAlias([string]$name, [string]$cmds)
    {
        $this.Name = $name
        $this.Cmds = $cmds
    }

    static [void] SaveAs([string]$name, [string]$cmds)
    {
        [CommandAlias]::List[$name] = [CommandAlias]::new($name, $cmds)
        #save
        $dir = Split-Path ([CommandAlias]::SaveLocation)
        if((Test-Path $dir) -eq $false)
        {
            New-Item -Path $dir -ItemType Directory
        }
        export-clixml -InputObject ([CommandAlias]::List) -path ([CommandAlias]::SaveLocation)
    }

    static [string[]] GetCommands([string]$name)
    {
        return ([CommandAlias]::List[$name]).Cmds -split ' '
    }

    static [bool] CommandAliasExists([string]$name)
    {
        return $null -ne [CommandAlias]::List[$name]
    }

    static [scriptblock] Dump()
    {
        return {
            [CommandAlias]::List | Format-Table -Property @{Expression=" ";Width=3}, @{Expression={$_.Name};Width=30}, @{Expression={$_.Value.Cmds};} -HideTableHeaders -Wrap
        }
    }
}

class MenuHelper
{
    static [Nullable[int]] ShowMenu([Array] $MenuItems,
    [ScriptBlock] $MenuItemFormatter,
    [Int] $Position)
    {
        if ($null -eq $MenuItemFormatter)
        {
            $MenuItemFormatter = { Param($M) $M.ToString() }
        }
        try{
            return Show-Menu -MenuItems $MenuItems -MenuItemFormatter $MenuItemFormatter -ReturnIndex -Position $Position
        }

        catch{
            return Show-Menu -MenuItems $MenuItems -MenuItemFormatter $MenuItemFormatter -ReturnIndex
        }
    }
}

class BaseProcess
{
    [string]$Repo
    [System.Collections.Queue]$Cmds

    hidden [scriptblock[]]$scripts
    [Command[]]$CommandList
    [string[]]$Containers

    BaseProcess([string[]]$cmds, [string[]]$containers)
    {
        $this.Repo = "BASE"
        $this.Cmds = [System.Collections.Queue]::new()
        foreach ($cmd in $cmds) {
            $this.Cmds.Enqueue($cmd)
        }
        $this.scripts = @()
        $this.CommandList = @(
            [Command]::new(@("<repo>"), $null, "Change the directory to the repo folder" ),
            [Command]::new(@("build","b"), $this.Build, "Build the repo of the current folder. Specifying the repo name will build without changing directory" ),
            [Command]::new(@("builddeps","bd"), $this.BuildDependencies, "Build the dependencies of the current folder. Specifying the repo name will build dependencies without changing directory" ),
            [Command]::new(@("buildlist","bl"), $this.BuildList, "Build and Publish a list of repos. This is useful for building repositories that rely on each other" ),
            [Command]::new(@("publish","p"), $this.Publish, "Publish the repo of the current folder. Specifying the repo name will publish without changing directory" ),
            [Command]::new(@("git","g"), $this.Git, "Usage git `"<args>`". Run a git command with the arguments. Use git help for a git documentation." ),
            [Command]::new(@("testing","t"), $this.Testing, "Checkout the testing branch" ),
            [Command]::new(@("develop","d"), $this.Develop, "Checkout the develop branch" ),
            [Command]::new(@("lastbranch","-"), $this.Lastbranch, "Checkout the last branch" ),
            [Command]::new(@("push"), $this.Push, "git push" ),
            [Command]::new(@("pull"), $this.Pull, "git pull" ),
            [Command]::new(@("fetch"), $this.Fetch, "git fetch --tags --prune" ),
            [Command]::new(@("merge"), $this.Merge, "Interactive merge to current branch" ),
            [Command]::new(@("switch"), $this.Switch, "Switch to a local branch" ),
            [Command]::new(@("checkout"), $this.Checkout, "Checkout a remote branch" ),
            [Command]::new(@("create-pr","pr"), $this.CreatePR, "Open PR on current branch" ),
            [Command]::new(@("stash"), $this.Stash, "git stash <name>" ),
            [Command]::new(@("stashpop"), $this.StashPop, "git stash pop <name>" ),
            [Command]::new(@("prunelocal"), $this.PruneLocal, "git delete all the local branches with no remote" ),
            [Command]::new(@("clone"), $this.Clone, "git clone the repo into the workspace folder" ),
            [Command]::new(@("cloneall"), $this.CloneAll, "git clone all the repos into the workspace folder" ),
            [Command]::new(@("edit","vs"), $this.Edit, "Open a solution in the repo" ),
            [Command]::new(@("code"), $this.Code, "Open a solution folder in vs code" ),
            [Command]::new(@("docker"), $this.Docker, "List the containers and their status" ),
            [Command]::new(@("docreset"), $this.DockerReset, "Reset the docker containers and networks" ),
            [Command]::new(@("start"), $this.DockerStart, "Start a container from a menu" ),
            [Command]::new(@("stop"), $this.DockerStop, "Stop a container from a menu" ),
            [Command]::new(@("saveas"), $this.SaveAs, "Usage: of saveas <name> ""<args>"". Save the arguments specified as a alias to use later" ),
            [Command]::new(@("help","h"), $this.Help, "Get the help docs" )
        )

        $this.Containers = $containers
    }

    [void] AddScriptFromCommand([string] $command)
    {
        $commandObj += ($this.CommandList | Where-Object CommandList -contains $command)
        if($null -ne $commandObj)
        {
            $this.scripts += $commandObj.GetScript()
        }
        else
        {
            $s = $this.GetCommandAlias($command);
            if($null -eq $s)
            {
                $this.scripts += {"$($command) is not a valid command. Try using help."}.GetNewClosure()
            }
            else {
                $this.scripts += $s
            }
        }
    }

    [scriptblock[]] GetCommandAlias($name)
    {
        [scriptblock[]] $scriptArr = @()
        $custCmds = [CommandAlias]::GetCommands($name)

        if([CommandAlias]::CommandAliasExists(($name)))
        {
            #change the directory so parsing commands doesn't pickup the current repo directory and can properly assign Cmds
            $thisDir = Get-Location
            Set-Location $global:wkspDir
            $repoCmds = [RepoDefinition]::ParseArgumentList($custCmds)

            #reset the dir
            Set-Location $global:thisDir
            foreach ($repoCmd in $repoCmds) {
                #need to check if the $repoCmd is a BaseProcess
                if($repoCmd.GetType() -eq [BaseProcess])
                {
                    #if it is, we need to cast it as the current type so repo commands work correctly
                    #use generate object by name for repo name and the Cmds of the current repoCmd
                    $castRepoCmd = [RepoDefinition]::GenerateObjectByName($this.Repo, $repoCmds.Cmds)
                    $scriptArr += $castRepoCmd.BuildScripts()
                }
                else
                {
                    $scriptArr += $repoCmd.BuildScripts()
                }
            }

            return $scriptArr
        }

        return $null
    }

    [scriptblock[]] BuildScripts()
    {
        
        if($this.Cmds.Count -gt 0) {
            $that = $this
            $sCmds = $this.Cmds.ToArray()
            $this.scripts += {
                utl.Write "Executing cmds on Repo $($that.Repo): $($sCmds)"
            }.GetNewClosure()
        }
        do {
            if($this.Cmds.Count -eq 0)
            {
                #No arguments, Change dir
                $this.AddScriptFromCommand("help")
                return $this.scripts
            }

            $next = $this.Cmds.Dequeue()
            $this.AddScriptFromCommand($next)
        } while ($this.Cmds.Count -gt 0)

        return $this.scripts
    }

    [scriptblock[]] Help()
    {
        $scriptArray = @()
        $scriptArray += $this.HelpUsage()
        $scriptArray += $this.HelpDescription()
        $scriptArray += $this.HelpRepos()
        $scriptArray += $this.HelpOptions()
        return $scriptArray
    }

    [scriptblock] HelpUsage()
    {
        return {
            "usage: of [repo] [<args>]"
            ""
        }
    }

    [scriptblock] HelpDescription()
    {
        return {
            "This is a command line utility to do various build and publish tasks for the OF repositories"
            ""
        }
    }

    [scriptblock] HelpRepos()
    {
        return {
            "Repositories"
            & $([RepoDefinition]::Dump())
        }
    }

    [scriptblock] HelpOptions()
    {
        $that = $this
        return {
            "Options"
            $that.CommandList | Format-Table -Property @{Expression=" ";Width=3}, @{Expression={$_.CommandList -join ', '};Width=30}, Description -HideTableHeaders -Wrap
            ""
            "Aliased Commands"
            & $([CommandAlias]::Dump())
        }.GetNewClosure()
    }

    [scriptblock] Build()
    {
        return { Write-Warning "Repo not specified" }
    }

    [scriptblock[]] BuildDependencies()
    {
        return { Write-Warning "Repo not specified" }
    }

    [scriptblock[]] BuildList()
    {
        $scriptArray = @()
        $help = {
            "usage: of buildlist <repos>"
            ""
            "Use this command to build multiple repositories in order"
            ""
            "repos       Comma seperated list of repo names. Ex. bol,core-service-clients,core,main"
            ""
        }

        if($this.Cmds.Count -eq 0)
        {
            $scriptArray += $help
            return $scriptArray
        }

        $next = $this.Cmds.Dequeue()
        switch($next)
        {
            help { $scriptArray += $help }

            default
            {
                $next -split " " | ForEach-Object {
                    $temp = [RepoDefinition]::GenerateObjectByName($_, @("b","p")).BuildScripts()
                    $temp | ForEach-Object { $scriptArray += $_ }
                }
            }
        }
        return $scriptArray
    }

    [scriptblock] Publish()
    {
        return { Write-Warning "Repo not specified" }
    }

    [scriptblock] Git() {
        $gitArgs = $this.Cmds.Dequeue()
        $splitString = [regex]::Split($gitArgs, ' (?=(?:[^"]|"[^"]*")*$)' )

        return {git @splitString}.GetNewClosure()
    }

    [scriptblock] Testing()
    {
        return { git checkout testing }
    }

    [scriptblock] Develop()
    {
        return { git checkout develop }
    }

    [scriptblock] Lastbranch()
    {
        return { git checkout - }
    }

    [scriptblock] Push()
    {
        return { git push }
    }

    [scriptblock] Pull()
    {
        return { git pull }
    }

    [scriptblock] Fetch()
    {
        return { git fetch --tags --prune }
    }

    [scriptblock] Merge()
    {
        return { Write-Warning "Repo not specified" }
    }

    [scriptblock] Switch()
    {
        return { Write-Warning "Repo not specified" }
    }

    [scriptblock] Checkout()
    {
        return { Write-Warning "Repo not specified" }
    }

    [scriptblock] CreatePR()
    {
        return { Write-Warning "Repo not specified" }
    }

    [scriptblock] Stash()
    {
        [string]$name = "temp"
        if($this.Cmds.Count -gt 0)
        {
            $name = [string]$this.Cmds.Dequeue()
        }
        return { git stash -m $name }.GetNewClosure()
    }

    [scriptblock] StashPop()
    {
        [string]$name = "temp"
        if($this.Cmds.Count -gt 0)
        {
            $name = [string]$this.Cmds.Dequeue()
        }
        return {
            $branch = git rev-parse --abbrev-ref HEAD
            $stash = git stash list --grep="^On $($branch): $($name)$" #search for an exact match on the message/name
            if($stash.Length -gt 0)
            {
                try {
                    [int]$pos = $stash.Substring(7,1) #get the number after stash@{
                    git stash pop $pos
                }
                catch {
                    Write-Warning "There was an error trying to pop stash $($name)"
                }
            }
        }.GetNewClosure()
    }

    [scriptblock] PruneLocal()
    {
        return {
            #get all local branches where the remote has been deleted and is not the current branch
            git branch -vv | where-object {$_ -like '*: gone]*' -and -not ($_[0] -eq '*')} | foreach-object {($_ -split ' ')[2]}
            $answer = Read-Host "Warning: This will delete the above local branches with no remote. This action cannot be undone. Are you sure? (y/n)"
            if($answer -eq 'y')
            {
                #delete these brnaches
                git branch -vv | where-object {$_ -like '*: gone]*' -and -not ($_[0] -eq '*')} | foreach-object {git branch -D ($_ -split ' ')[2]}
            }
        }
    }

    [scriptblock[]] Clone()
    {
        return { Write-Warning "Repo not specified" }
    }

    [scriptblock[]] CloneAll()
    {
        $scriptArray = @()
        [RepoDefinition]::List | ForEach-Object {
            $temp = $_.GenerateObject(@("clone")).BuildScripts()
            $temp | ForEach-Object { $scriptArray += $_ }
        }

        return $scriptArray
    }

    [scriptblock] Edit()
    {
        [int]$pos = -1
        if($this.Cmds.Count -gt 0)
        {
            $pos = [int]$this.Cmds.Dequeue()
        }
        return {
            #check for .git folder
            if((Get-ChildItem -Directory -Hidden -Filter .git).Count -eq 0)
            {
                "This command can only be run in a repo folder."
            }
            else
            {
                # get all the solutions in the folder
                $solutionsCacheKey = "SolutionMenu-$(Split-Path -Path (Get-Location) -Leaf)"
                $solutions = [FileCache]::Get($solutionsCacheKey)
                if($null -eq $solutions)
                {
                    $solutions = Get-ChildItem *.sln -Recurse
                                    | Select-Object FullName, @{Name="FolderDepth";Expression={$_.DirectoryName.Split('\').Count}}, Name
                                    | Sort-Object FolderDepth,FullName

                    [FileCache]::Set($solutionsCacheKey, $solutions, $repoMenuCacheExpireMin)
                }

                #none, Print message
                if($solutions.Count -eq 0)
                {
                    "There are no solutions in this repo."
                }
                elseif ($solutions.Count -eq 1)
                {
                    Start-Process $solutions.FullName
                }
                else
                {
                    # more than 1
                    if($pos -gt -1)
                    {
                        Start-Process $solutions[$pos].FullName
                    }
                    else
                    {
                        #User did not provide selection, create a menu
                        $selectionCacheKey = "SolutionSelection-$(Split-Path -Path (Get-Location) -Leaf)"
                        $selection = [FileCache]::Get($selectionCacheKey)
                        $selection = [MenuHelper]::ShowMenu($solutions, { $Args | Select-Object -Exp Name}, $selection)

                        if($null -ne $selection) {
                            Start-Process $solutions[$selection].FullName
                            [FileCache]::Set($selectionCacheKey, $selection, $repoMenuCacheExpireMin)
                        }
                    }
                }
            }}.GetNewClosure()
    }

    [scriptblock] Code()
    {
        return {
            code .
        }
    }

    [scriptblock] Docker()
    {
        $that = $this
        return {

            $containerStatuses = $that.GetDockerContainers().Invoke()

            foreach ($containerStatus in $containerStatuses) {
                "`t$($containerStatus.Name)"
            }
        }.GetNewClosure()
    }

    [scriptblock[]] DockerReset()
    {
        $that = $this
        return {
            $temp = $that.DockerCheck().Invoke()
            $curDir = Get-Location

            Set-Location $global:ofDockerDir
            docker compose down
            Set-Location $curDir
        }.GetNewClosure()
    }

    [scriptblock] DockerStart()
    {
        $that = $this
        return {

            $containerStatuses = $that.GetDockerContainers().Invoke()
            $selection = [MenuHelper]::ShowMenu($containerStatuses, { $Args | Select-Object -Exp Name}, $null)

            if($null -ne $selection) {
                $curDir = Get-Location

                Set-Location $global:ofDockerDir
                docker compose up $containerStatuses[$selection].Value -d
                Start-Sleep -seconds 2
                start "http://localhost:$($containerStatuses[$selection].Port)"
                Set-Location $curDir
            }

        }.GetNewClosure()
    }

    [scriptblock] DockerStop()
    {
        $that = $this
        return {

            $containerStatuses = $that.GetDockerContainers().Invoke()
            $selection = [MenuHelper]::ShowMenu($containerStatuses, { $Args | Select-Object -Exp Name}, $null)

            if($null -ne $selection) {
                $curDir = Get-Location

                Set-Location $global:ofDockerDir
                docker compose stop $containerStatuses[$selection].Value
                Set-Location $curDir
            }

        }.GetNewClosure()
    }

    [scriptblock[]] GetDockerContainers()
    {
        $that = $this
        return {

            $temp = $that.DockerCheck().Invoke()
            $curDir = Get-Location

            Set-Location $global:ofDockerDir
            $yml = Get-Content .\docker-compose.yml -Raw
            $compose = ConvertFrom-YAML $yml

            $containerStatuses = @()
            #loop through
            foreach ($service in $compose.services.GetEnumerator()) {
                if($service.Value.labels.Count -gt 0)
                {
                    #parse the labels
                    $labels = @{}

                    foreach ($label in $service.Value.labels) {
                        $pos = $label.IndexOf("=")
                        $name = $label.Substring(0, $pos)
                        $value = $label.Substring($pos+1, $label.Length - $pos-1)
                        $labels[$name] = $value
                    }
                    $serviceId = $labels["service-id"]
                    #filter out containers for current repo if there are any

                    if($that.Containers.Count -gt 0 -and (-not $that.Containers.Contains($serviceId)))
                    {
                        #skip listing this container
                        continue
                    }

                    #find the running service
                    $container = docker ps -f "label=service-id=$($serviceId)" -a --format "{{.ID}}={{.Status}}"
                    $containerid = ""
                    $containerstatus = "Not Created"
                    $containername = $labels["name"]
                    if($container.Length -gt 0)
                    {
                        $pos = $container.IndexOf("=")
                        $containerid = $container.Substring(0, $pos)
                        $containerstatus = $container.Substring($pos+1, $container.Length - $pos-1)
                    }

                    #get the port
                    $portAlias = $service.Value.ports[0]
                    $port = ""
                    if($portAlias.Length -gt 0)
                    {
                        $pos = $portAlias.IndexOf(":")
                        $port = $portAlias.Substring(0, $pos)
                    }


                    $containerStatuses += [PSCustomObject]@{
                        Name="$($containername) [$($containerstatus)]"
                        Value=$serviceId
                        Port=$port
                    }
                }

            }

            Set-Location $curDir

            return $containerStatuses
        }.GetNewClosure()
    }

    [scriptblock[]] DockerCheck() {
        return {
            #confirm Docker Desktop is running in windows mode. Attempt to start if possible
            if(-not (Test-Path "$($Env:ProgramFiles)\Docker\Docker\Docker Desktop.exe"))
            {
                Write-Warning 'Docker Desktop is not installed. Refer to the README.md for install steps'
                return $false
            }
            if((docker ps 2>&1).GetType() -eq [System.Management.Automation.ErrorRecord]){
                #docker is not running
                Write-Host "Starting Docker Desktop..."
                net start com.docker.service
                Start-Process "$($Env:ProgramFiles)\Docker\Docker\Docker Desktop.exe"

                while((docker ps 2>&1).GetType() -eq [System.Management.Automation.ErrorRecord])
                {
                    Start-Sleep -Seconds 1
                }
            }

            $dockerRuntimeOS = docker info --format "{{.OSType}}"
            if($dockerRuntimeOS -ne "windows")
            {
                if(-not (Test-Path "$($Env:ProgramFiles)\Docker\Docker\DockerCli.exe"))
                {
                    Write-Warning 'Docker must be running in Windows mode. Right-click the docker tray icon and select "Switch to Windows containers..."'
                    return $false
                }
                Write-Host 'Attempting to switch to windows mode'
                & "$($Env:ProgramFiles)\Docker\Docker\DockerCli.exe" -SwitchDaemon

                #wait for containers to be available
                [int]$count = docker info --format "{{.Containers}}"
                [int]$total = (docker ps -a --format "{{.ID}}").Count
                if($count -ne $total)
                {
                    do{
                        Start-Sleep -Seconds 1
                        [int]$count = docker info --format "{{.Containers}}"
                        [int]$total = (docker ps -a --format "{{.ID}}").Count
                    }while($count -ne $total)
                }
            }

            return $true
        }
    }

    [scriptblock[]] SaveAs()
    {
        $name = $this.Cmds.Dequeue()
        $custCmds = $this.Cmds.Dequeue()
        return {
            [CommandAlias]::SaveAs($name, $custCmds)
        }.GetNewClosure()
    }
}

class LibRepository : BaseProcess
{
    hidden [string]$LastDirectory
    [string]$Directory
    [string[]]$Dependencies
    [string]$CloneSSH
    LibRepository([string[]]$cmds, [string]$repo, [string]$directory, [string]$cloneSSH, [string[]]$dependencies, [string[]]$containers) : base($cmds, $containers)
    {
        $this.Repo = $repo
        $this.Directory = $directory
        $this.Dependencies = $dependencies
        $this.CloneSSH = $cloneSSH
    }

    [scriptblock[]] BuildScripts()
    {
        $that = $this
        $this.LastDirectory = Get-Location
        $this.scripts += {
            if(-not (Test-Path $that.Directory))
            {
                mkdir $that.Directory
            }

            Set-Location $that.Directory
        }.GetNewClosure()

        if($that.Cmds.Count -eq 0)
        {
            if((Join-Path $this.LastDirectory '') -eq (Join-Path $this.Directory ''))
            {
                $this.AddScriptFromCommand("help")
            }
            #No arguments, Change dir
            return $this.scripts
        }

        ([BaseProcess]$this).BuildScripts()

        $this.scripts += {
            Set-Location $that.LastDirectory
        }.GetNewClosure()

        return $this.scripts
    }

    [scriptblock[]] BuildDependencies()
    {
        $that = $this;
        if($this.Dependencies.Length -eq 0)
        {
            return { "There are no dependencies for $($that.Repo)" }.GetNewClosure()
        }
        $scriptArray = @()
        $this.Dependencies -split " " | ForEach-Object {
            $temp = [RepoDefinition]::GenerateObjectByName($_, @("b","p")).BuildScripts()
            $temp | ForEach-Object { $scriptArray += $_ }
        }

        return $scriptArray
    }

    [scriptblock] Build()
    {
        $that = $this
        return {

            $test = $that.DockerCheck().Invoke()
            if($test)
            {
                foreach ($container in $that.Containers)
                {
                    #stop any running containers before the build
                    if($null -ne $container) {
                        $curDir = Get-Location

                        Set-Location $global:ofDockerDir
                        docker compose stop $container
                        Set-Location $curDir
                    }
                }
            }
            # utl.Write "Cleaning $($that.Repo)"
            # utl.clean.AllPackages
            utl.Write "Building $($that.Repo)"

            # Remove/Restore NuGet.Config
            utl.remove.File .\NuGet.Bak
            git restore .\NuGet.Config

            $null = Start-Transcript "$($env:TEMP)\OFPowershell\build.txt"
            of.Build
            $null = Stop-Transcript
            $output = Get-Content -Path "$($env:TEMP)\OFPowershell\build.txt" -Tail 7
            if($output -contains "Error: One or more errors occurred.")
            {
                #stop the rest of the command chain
                #make sure to return to the original directory
                Set-Location $that.LastDirectory
                Write-Error "$($that.Repo) Build Failed"
                throw "$($that.Repo) Build Failed"
            }

        }.GetNewClosure()
    }

    [scriptblock] Publish()
    {
        $that = $this
        return {
            utl.Write "Publishing $($that.Repo)"
            of.Publish
        }.GetNewClosure()
    }

    [scriptblock] HelpUsage()
    {
        $that = $this
        return {
            "usage: of $($that.Repo) [<args>]"
            ""
        }.GetNewClosure()
    }

    [scriptblock] Merge()
    {
        $that = $this
        return {
            $curBranch = git rev-parse --abbrev-ref HEAD
            $branches = git branch

            if($branches.Length -gt 0)
            {
                for($i = 0; $i -lt $branches.Length; $i+=1)
                {
                    $branches[$i] = $branches[$i].Trim()
                    $branches[$i] = $branches[$i] -replace "^\+ ",""
                    $branches[$i] = $branches[$i] -replace "^\* ",""
                }

                $branches = $branches | Where-Object {$_ -ne $curBranch}

                "Select the branch to merge with branch $($curBranch)"
                $selection = [MenuHelper]::ShowMenu($branches, $null, $null)

                git merge $branches[$selection]
            }
            else {
                "No branches exist for $($that.Repo)"
            }
        }.GetNewClosure()
    }

    [scriptblock] Switch()
    {
        $that = $this
        return {
            $curBranch = git rev-parse --abbrev-ref HEAD
            $branches = git branch

            if($branches.Length -gt 0)
            {
                for($i = 0; $i -lt $branches.Length; $i+=1)
                {
                    $branches[$i] = $branches[$i].Trim()
                    $branches[$i] = $branches[$i] -replace "^\+ ",""
                    $branches[$i] = $branches[$i] -replace "^\* ",""
                }

                $branches = $branches | Where-Object {$_ -ne $curBranch}

                "Select the branch you want to switch to"
                $selection = [MenuHelper]::ShowMenu($branches, $null, $null)

                git switch $branches[$selection]
            }
            else {
                "No branches exist for $($that.Repo)"
            }
        }.GetNewClosure()
    }

    [scriptblock] Checkout()
    {
        $that = $this
        return {
            $curBranch = git rev-parse --abbrev-ref HEAD
            $branches = git branch -r

            if($branches.Length -gt 0)
            {
                for($i = 0; $i -lt $branches.Length; $i+=1)
                {
                    $branches[$i] = $branches[$i].Trim()
                    $branches[$i] = $branches[$i] -replace "^\+ ",""
                    $branches[$i] = $branches[$i] -replace "^\* ",""
                }

                $branches = $branches | Where-Object {$_ -ne $curBranch}

                "Select the branch you want to checkout"
                $selection = [MenuHelper]::ShowMenu($branches, $null, $null)

                git checkout $branches[$selection].Replace("origin/", "")
            }
            else {
                "No branches exist for $($that.Repo)"
            }
        }.GetNewClosure()
    }

    [scriptblock[]] Clone()
    {
        $that = $this
        return {
            git clone $that.CloneSSH .
        }.GetNewClosure()
    }

    [scriptblock[]] CreatePR()
    {
        $that = $this
        return {
            $currBranch = git rev-parse --abbrev-ref HEAD
            $url = $that.CloneSSH.Replace(".git", "")
            $url = $url + "/pull-requests/new?source=" + $currBranch

            start $url
        }.GetNewClosure()
    }
}

class ApplicationRepository : LibRepository
{
    ApplicationRepository([string[]]$cmds, [string]$repo, [string]$directory, [string]$cloneSSH, [string[]]$dependencies, [string[]]$containers) :
        base($cmds, $repo, $directory, $cloneSSH, $dependencies, $containers)
    {
    }

    [scriptblock] Publish()
    {
        return {Write-Warning "This repo does not have a Publish target"}
    }
}

New-Module chain-commands {

    #Entry point.
    function of {
        $repoCmds = [RepoDefinition]::ParseArgumentList($args)
        foreach ($repoCmd in $repoCmds) {
            try {
                executeScripts -scripts $repoCmd.BuildScripts()
            }
            catch {
                break
            }
        }
    }

    function executeScripts {
        param([scriptblock[]]$scripts)
        foreach($script in $scripts)
        {
            & $($script)
        }
    }

    Export-ModuleMember of

} | Import-Module