# Quick Order Facilitator commands.

New-Module of-commands {

# Functions ---------------------------
  # Clean/Fix commands.
  function of.Clean {
    utl.remove.Path .\Packages\Sci.Of.*
  }
  function of.clean.Packages {
    utl.Write "Removing Sci.Of packages from 'of-core-service-clients'"
    Remove-Item ${ofDevDir}\of-core-service-clients\Packages\Sci.Of.* -Recurse -Force
    utl.Write "Removing Sci.Of packages from 'of-core'"
    Remove-Item ${ofDevDir}\of-core\Packages\Sci.Of.* -Recurse -Force
    utl.Write "Removing Sci.Of packages from 'of-main'"
    Remove-Item ${ofDevDir}\of-main\Packages\Sci.Of.* -Recurse -Force
  }

  # Build/Publish commands.
  function of.Build {
    .\build.ps1 -configuration debug
    g.Restore.AssemblyInfo
  }
  function of.Publish {
    .\build.ps1 -target "Publish-Local"
  }
  function of.FullPublish {
    of.Build;
    of.Publish;
  }
  function of.lib.FullPublish {
    utl.Write "-[ GitVersion Fix ]-"
    utl.clean.GitVersion
    utl.Write "-[ Cleaning Packages ]-"
    of.clean.Packages

    of.dev.Bol
    utl.Write "-[ FullPublish: of-bol ]-"
    of.FullPublish

    of.dev.CoreService
    utl.Write "-[ FullPublish: of-core-service-clients ]-"
    of.FullPublish

    of.dev.Core
    utl.Write "-[ FullPublish: of-core ]-"
    of.FullPublish
  }

  # Quick fetch all projects.
  # Gross and large. Need to clean up.
  function of.g.FetchAll {
    of.dev.Log
    utl.Write "fetching: of-logging"
    g.Fetch

    of.dev.Bol
    utl.Write "fetching: of-bol"
    g.Fetch

    of.dev.CoreService
    utl.Write "fetching: of-core-service-clients"
    g.Fetch

    of.dev.Fax
    utl.Write "fetching: of-fax"
    g.Fetch

    of.dev.FaxService
    utl.Write "fetching: of-fax-image-service"
    g.Fetch

    of.dev.ScimsCore
    utl.Write "fetching: of-scims.core"
    g.Fetch

    of.dev.Scims
    utl.Write "fetching: of-scims"
    g.Fetch

    of.dev.Ice
    utl.Write "fetching: of-ice"
    g.Fetch

    of.dev.ITools
    utl.Write "fetching: of-iTools"
    g.Fetch

    of.dev.Core
    utl.Write "fetching: of-core"
    g.Fetch

    of.dev.Main
    utl.Write "fetching: of-main"
    g.Fetch
  }

  # Quick checkout develop all projects.
  # Gross and large. Need to clean up.
  function of.g.Develop {
    of.dev.Log
    utl.Write "checkout develop: of-logging"
    g.Checkout develop

    of.dev.Bol
    utl.Write "checkout develop: of-bol"
    g.Checkout develop

    of.dev.CoreService
    utl.Write "checkout develop: of-core-service-clients"
    g.Checkout develop

    of.dev.Fax
    utl.Write "checkout develop: of-fax"
    g.Checkout develop

    of.dev.FaxService
    utl.Write "checkout develop: of-fax-image-service"
    g.Checkout develop

    of.dev.ScimsCore
    utl.Write "checkout develop: of-scims.core"
    g.Checkout develop

    of.dev.Scims
    utl.Write "checkout develop: of-scims"
    g.Checkout develop

    of.dev.Ice
    utl.Write "checkout develop: of-ice"
    g.Checkout develop

    of.dev.ITools
    utl.Write "checkout develop: of-iTools"
    g.Checkout develop

    of.dev.Core
    utl.Write "checkout develop: of-core"
    g.Checkout develop

    of.dev.Main
    utl.Write "checkout develop: of-main"
    g.Checkout develop
  }

  function of.Order {
    utl.Write "

    - logging
    - fax-image-service
    - bol
    - fax
    - core-service-clients
    - core
    - scims.core
    - scims
    - ice
    - iTools
    - main"
  }
# Functions ---------------------------


# Exports -----------------------------
  Export-ModuleMember of.Clean
  Export-ModuleMember of.clean.Packages

  Export-ModuleMember of.Build
  Export-ModuleMember of.Publish
  Export-ModuleMember of.FullPublish
  Export-ModuleMember of.lib.FullPublish

  Export-ModuleMember of.g.FetchAll
  Export-ModuleMember of.g.Develop
  Export-ModuleMember of.Order
# Exports -----------------------------

} | Import-Module