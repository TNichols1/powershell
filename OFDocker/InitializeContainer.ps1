If (Test-Path Env:\PreInitScript)
{
    powershell $env:PreInitScript
}

If (Test-Path Env:\TRANSFORM_CONFIG)
{
    \WebConfigTransformRunner.1.0.0.1\Tools\WebConfigTransformRunner.exe \inetpub\wwwroot\Web.config $env:TRANSFORM_CONFIG \inetpub\wwwroot\Web.config
}

[string]$sourceDirectory  = "C:\seq_logging\*"
[string]$destinationDirectory = "C:\inetpub\wwwroot\bin\"
Copy-item -Force -Recurse -Verbose $sourceDirectory -Destination $destinationDirectory

# Inject environment variables into Web.config
$string = Get-Content -Path \inetpub\wwwroot\Web.config -Raw

$string = [System.Environment]::ExpandEnvironmentVariables($string)

Set-Content -Path \inetpub\wwwroot\Web.config -Value $string